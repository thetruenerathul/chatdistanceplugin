package de.nerathul.lezora.ChatDistance;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class Main extends JavaPlugin implements Listener {

    public static List<ChatEntity> ingamePlayer = new ArrayList<ChatEntity>();
    public static Chatroom global = new Chatroom("Welt", 0);
    public static Chatroom help = new Chatroom("Hilfe", 0);
    public static Chatroom shout = new Chatroom("ruft", 100, ChatColor.AQUA);
    public static Chatroom speak = new Chatroom("spricht", 15, ChatColor.GREEN);
    public static Chatroom wisper = new Chatroom("flüstert", 2, ChatColor.LIGHT_PURPLE);

    @Override
    public void onEnable() {
        getLogger().info("DistanceChat is enabled!");
        this.getServer().getPluginManager().registerEvents(this, this);
        this.getConfig();

    }

    @Override
    public void onDisable() {
        getLogger().info("DistanceChat got disabled!");
    }

    public void loadData() {

    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        switch (label)
        {

            case "welt":
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    for (ChatEntity entity: ingamePlayer) {
                        if(entity.getPlayer() == player) {
                            player.sendMessage(ChatColor.GOLD + "Du schreibst jetzt in: " + ChatColor.AQUA + "Welt");
                            entity.setRoom(global);
                            break;
                        }
                    }
                    return true;
                }
                break;
            case "rufen":
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    for (ChatEntity entity: ingamePlayer) {
                        if(entity.getPlayer() == player) {
                            player.sendMessage(ChatColor.GOLD + "Du schreibst jetzt in: " + ChatColor.AQUA + "rufen");
                            entity.setRoom(shout);
                            break;
                        }
                    }
                    return true;
                }
                break;
            case "sprechen":
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    for (ChatEntity entity: ingamePlayer) {
                        if(entity.getPlayer() == player) {
                            player.sendMessage(ChatColor.GOLD + "Du schreibst jetzt in: " + ChatColor.GREEN + "Sprechen");
                            entity.setRoom(speak);
                            break;
                        }
                    }
                    return true;
                }
                break;
            case "flüstern":
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    for (ChatEntity entity: ingamePlayer) {
                        if(entity.getPlayer() == player) {
                            player.sendMessage(ChatColor.GOLD + "Du schreibst jetzt in: " + ChatColor.LIGHT_PURPLE + "Flüstern");
                            entity.setRoom(wisper);
                            break;
                        }
                    }
                    return true;
                }
                break;
            case "hilfe":
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    for (ChatEntity entity: ingamePlayer) {
                        if(entity.getPlayer() == player) {
                            player.sendMessage(ChatColor.GOLD + "Du schreibst jetzt in: " + ChatColor.DARK_RED + "Hilfe");
                            entity.setRoom(help);
                            break;
                        }
                    }
                    return true;
                }
                break;
            case "stumm":
                if(args.length > 0) {
                    ChatEntity entity = null;
                    if(sender instanceof Player) {
                        Player player = (Player) sender;
                        for(ChatEntity e : ingamePlayer) {
                            if(e.getPlayer().equals(player)) {
                                entity = e;
                            }
                        }
                    }
                    Player player = (Player) sender;
                    if(args[0].equals("welt")) {
                        entity.global = !entity.global;
                        if(entity.global) {
                            player.sendMessage(ChatColor.GOLD + "Du hörst die Welt wieder");
                        } else {
                            player.sendMessage(ChatColor.GOLD + "Die Welt erstummt");
                        }
                    }else if(args[0].equals("hilfe")){
                        entity.help = !entity.help;
                        if(entity.help) {
                            player.sendMessage(ChatColor.GOLD + "Du hörst die Hilfesuchenden wieder");
                        } else {
                            player.sendMessage(ChatColor.GOLD + "Die Hilfeschreie verstummen");
                        }
                    }else{
                        sender.sendMessage("Falscher Eingabewert");
                        return false;
                    }
                }else {
                    sender.sendMessage("kein Parameter angegeben");
                    return false;
                }
        }

        return false;
    }


    //Spieler beim Joinen im plugin regestrieren
    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        ChatEntity entity = new ChatEntity(e.getPlayer());
        ingamePlayer.add(entity);
    }


    //Spieler beim verlassen wieder aus der Spielerliste entfernen
    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        for(ChatEntity entity : ingamePlayer) {
            if(entity.getPlayer().equals(player)) {
                ingamePlayer.remove(entity);
                break;
            }
        }
    }

    //Spieler der schreibt finden und in chatEntity umwandeln
    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        Player player = e.getPlayer();
        ChatEntity playerEntity = null;
        for(ChatEntity entity : ingamePlayer) {
            if(entity.getPlayer() == player) {
                playerEntity = entity;
                break;
            }
        }

        //Wenn nicht im globalchat gesprochen werden soll Nachricht umlenken
        if(playerEntity.getRoom() != global && playerEntity.getRoom() != help) {
            e.setCancelled(true);
            playerEntity.sendMessage(playerEntity.getRoom().color, e.getMessage());
        }
        if(playerEntity.getRoom() == global) {
            playerEntity.sendGlobal(e.getMessage());
            e.setCancelled(true);
        }
        if(playerEntity.getRoom() == help) {
            playerEntity.sendHelp(e.getMessage());
            e.setCancelled(true);
        }
    }

}
