package de.nerathul.lezora.ChatDistance;

import org.bukkit.ChatColor;

public class Chatroom {
    int range;
    String chatName;
    ChatColor color = ChatColor.GRAY;

    public Chatroom(String chatName, int range) {
        this.chatName = chatName;
        this.range = range;
    }

    public Chatroom(String chatName, int range, ChatColor color) {
        this.chatName = chatName;
        this.range = range;
        this.color = color;
    }
}
