package de.nerathul.lezora.ChatDistance;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ChatEntity{

    private Player player;
    private Chatroom room;
    boolean global = true;
    boolean help = true;
    private String nickName;

    public ChatEntity(Player player) {
        this.player = player;
        this.room = Main.global;
    }

    //Alle Spieler in der Reichweite des passenden Raums finden
    private List<Player> getNearPlayers(ChatEntity entity) {
        List nearPlayers = new ArrayList<Player>();
        World world = entity.getPlayer().getWorld();
        List<Player> playerList = world.getPlayers();
        for (Player playerInRange: playerList) {
            double distance = playerInRange.getLocation().distance(entity.getPlayer().getLocation());
            if(distance <= entity.getRoom().range) {
                nearPlayers.add(playerInRange);
            }
        }
        return nearPlayers;
    }

    //Nachricht an Alle die in Reichweite sind schicken
    public void sendMessage(ChatColor color, String msg) {
        List<Player> nearPlayers = getNearPlayers(this);
        for(Player nearPlayer : nearPlayers) {
            nearPlayer.sendMessage(color + "" + this.player.getPlayer().getName() + " " + this.room.chatName + ": " + ChatColor.WHITE + "" + msg);
        }
    }

    public void sendGlobal(String msg) {
        for(ChatEntity player : Main.ingamePlayer) {
            if(player.global) {
                player.getPlayer().sendMessage(ChatColor.AQUA + "[Welt] " + ChatColor.GOLD + " " + this.player.getName() + ": " + ChatColor.WHITE + msg);
            }
        }
    }

    public void sendHelp(String msg) {
        for(ChatEntity player : Main.ingamePlayer) {
            if(player.help) {
                player.player.sendMessage(ChatColor.DARK_RED + "[Hilfe] " + ChatColor.GOLD + " " + this.player.getName() + ": " + ChatColor.WHITE + msg);
            }
        }
    }

    public void setRoom(Chatroom room) {
        this.room = room;
    }

    public Chatroom getRoom() {
        return this.room;
    }

    public Player getPlayer() {
        return this.player;
    }

}
